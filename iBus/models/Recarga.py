from django.db import models
from . import sTarjeta

class sEstatusRecarga(models.Model):
    nombre = models.CharField(max_length=25)
    descripcion = models.CharField(max_length=50)


class sSucursal(models.Model):
    nombre = models.CharField(max_length=50)
    razonSocial = models.CharField(max_length=75)
    latitud = models.FloatField()
    longitud = models.FloatField()


class sCajero(models.Model):
    # añadir datos extra a un cajero, se analiza opcion de añador relacion con usuario
    nombre = models.CharField(max_length=50)
    idSucursal = models.ForeignKey(sSucursal, on_delete=models.SET_NULL, null=True)


class sRecarga(models.Model):
    monto = models.IntegerField()
    # relationship
    idTarjeta = models.ForeignKey(sTarjeta, on_delete=models.SET_NULL, null=True)
    idCajero = models.ForeignKey(sCajero, on_delete=models.SET_NULL, null=True)
    idEstatusRecarga = models.ForeignKey(
        sEstatusRecarga, on_delete=models.SET_NULL, null=True)
