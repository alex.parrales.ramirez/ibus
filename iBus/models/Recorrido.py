from django.db import models
from . import sConductor, sVehiculo, sGrupoRuta

class sRecorridoEstatus(models.Model):
    aNombre = models.CharField(max_length=25)
    aDescripcion = models.CharField(max_length=25)
    class Meta:
        db_table = 'sRecorridoEstatus'

class sRecorridoUbicacion(models.Model):
    aLatitud = models.DecimalField(max_digits=10, decimal_places=6)
    aLongitud = models.DecimalField(max_digits=10, decimal_places=6)

    aCreated_at = models.DateTimeField(auto_now_add=True)
    aUpdated_at = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'sRecorridoUbicacion'

class sRecorrido(models.Model):
    aHoraInicio = models.DateTimeField()
    aHoraFin = models.DateTimeField()
    aHoraInicioReal = models.DateTimeField(blank=True)
    aHoraFinReal = models.DateTimeField(blank=True)
    aCreated_at = models.DateTimeField(auto_now_add=True)
    aUpdated_at = models.DateTimeField(auto_now=True)

    RecorridoEstatusID = models.ForeignKey(sRecorridoEstatus, on_delete=models.CASCADE, db_column='RecorridoEstatusID')
    ConductorID = models.ForeignKey(sConductor, on_delete=models.CASCADE, db_column='ConductorID')
    VehiculoID = models.ForeignKey(sVehiculo, on_delete=models.CASCADE, db_column='VehiculoID')
    GrupoRutaID = models.ForeignKey(sGrupoRuta, on_delete=models.CASCADE, db_column='GrupoRutaID')
    
    class Meta:
        db_table = 'sRecorrido'

