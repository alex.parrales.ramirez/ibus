from django.db import models

class sEstacion(models.Model):
    aNombre = models.CharField(max_length=50)
    aLatitud = models.DecimalField(max_digits=10, decimal_places=6)
    aLongitud = models.DecimalField(max_digits=10, decimal_places=6)
    aCreated_at = models.DateTimeField(auto_now_add=True)
    aUpdated_at = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'sEstacion'

class sRuta(models.Model):
    aNombre = models.CharField(max_length=25)
    aDescripcion = models.CharField(max_length=255,null=True)
    aCreated_at = models.DateTimeField(auto_now_add=True)
    aUpdated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        db_table = 'sRuta'

class sGrupoRuta(models.Model):
    CHOICE_TIPO = (
        ('A', 'Tipo A'),
        ('B', 'Tipo B')
    )
    aTipo = models.CharField(max_length=1, choices=CHOICE_TIPO, default="A")
    
    RutaID = models.ForeignKey(sRuta, on_delete=models.CASCADE, db_column="RutaID")

    class Meta:
        db_table = 'sGrupoRuta'

class sEstacionRuta(models.Model):
    GrupoRutaID = models.ForeignKey(sGrupoRuta, on_delete=models.CASCADE, db_column='GrupoRutaID')
    EstacionID = models.ForeignKey(sEstacion, on_delete=models.CASCADE, db_column='EstacionID')
    class Meta:
        db_table = 'sEstacionRuta'

class sTraza(models.Model):
    aLatitud = models.DecimalField(max_digits=10, decimal_places=6)
    aLongitud = models.DecimalField(max_digits=10, decimal_places=6)
    
    GrupoRutaID = models.ForeignKey(sGrupoRuta, on_delete=models.CASCADE, db_column="GrupoRutaID")

    class Meta:
        db_table = 'sTraza'