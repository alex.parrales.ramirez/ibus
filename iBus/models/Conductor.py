from django.db import models

from .User import User

class sConductor(models.Model):
    tarjeta = models.CharField(max_length=50)
    numeroLicencia = models.CharField(max_length=25, unique=True)
    idUsuario = models.ForeignKey(User, on_delete=models.CASCADE, db_column="idUsuario")
    class Meta:
        db_table = 'sConductor'
