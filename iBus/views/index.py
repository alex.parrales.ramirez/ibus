from django.shortcuts import render
from django.views.generic import CreateView

class Index(CreateView):
    template_name = 'iBus/index.html'

    def get(self, request):
        return render(request, 'iBus/index.html')
