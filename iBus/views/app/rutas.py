from django.shortcuts import render
from django.views.generic import CreateView
from iBus.models import *
from django.http import JsonResponse

class Rutas(CreateView):
    template_name = 'iBus/app/rutas.html'
    def get(self, request):
        rutasQuerySet = sRuta.objects.all()
        rutas = []
        for ruta in rutasQuerySet:
            rutas.append({
                'id': ruta.id,
                'nombre': ruta.nombre,
            })

        cntx = {
            'rutas': rutas
        }
        return render(request, self.template_name, cntx)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get('operation') == 'getRuta':
                grupoRuta = sGrupoRuta.objects.filter(idRuta=request.POST.get('idRuta')).values('id', 'tipo')
                rutas = []
                for ruta in grupoRuta:
                    # Obtiene las trazas de la ruta segun su tipo
                    trazasQuerySet = sTrazaRuta.objects.filter(idGrupoRuta=ruta['id']).values('latitud', 'longitud')
                    trazas = []
                    for traza in trazasQuerySet:
                        trazas.append({
                            'latitud': traza['latitud'],
                            'longitud': traza['longitud']
                        })
                    # Obtiene las estaciones de la ruta segun su tipo
                    estacionesQuerySet = sEstacionRuta.objects.filter(idGrupoRuta=ruta['id'])
                    estaciones = []
                    for estacion in estacionesQuerySet:
                        estaciones.append({
                            "nombre" : estacion.idEstacion.nombre,
                            "latitud": estacion.idEstacion.latitud,
                            "longitud" : estacion.idEstacion.longitud
                        })
                    rutas.append({
                        'tipo': ruta['tipo'],
                        'trazas' : trazas,
                        'estaciones' : estaciones
                    })
            cntx = {
                "rutas" : rutas
            }
            return JsonResponse(cntx)