from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.views.generic import View
from iBus.models import User

class Register(View):
    def get(self, request):
        return render(request, 'iBus/register.html')

    def post(self, request):
        if request.method == 'POST':
            user = User(
                CURP=request.POST.get('CURP'),
                nombre=request.POST.get('nombre'),
                apellidoPaterno=request.POST.get('apellidoPaterno'),
                apellidoMaterno=request.POST.get('apellidoMaterno'),
                sexo='M',#request.POST.get('sexo'),
                fechaNacimiento= '1996-12-17',       #request.POST.get('fechaNacimiento'),
                nacionalidad= 'Mexicana', #request.POST.get('nacionalidad'),
                correoElectronico=request.POST.get('correoElectronico'),
                telefono=request.POST.get('telefono'),
                celular=request.POST.get('celular'),
            )
            user.save()
            user.set_password(request.POST.get('password'))
            user.save()
            user = authenticate(CURP=request.POST.get('CURP'), password=request.POST.get('password'))
            login(request, user)
            return render(request, 'iBus/index.html')
