from django.contrib.auth import login, authenticate
from django.shortcuts import render
from django.views.generic import CreateView
from iBus.models import sConductor, User
from django.http import JsonResponse

class ConductorConfig(CreateView):
    template_name = "config/conductor.html"

    def get(self, request):
        if not request.user.is_authenticated:
            return render(request, 'iBus/errors/403.html')
        else:
            pFlag = False
            return render(request, self.template_name)
        
    def post(self, request):
        if not request.user.is_authenticated:
            return render(request, '403.html')
        else:

            if request.POST.get('operation') == 'read':
                conductoresQuery = sConductor.objects.all()
                conductores = []
                for conductor in conductoresQuery:
                    conductores.append({
                        'id': conductor.id,
                        'idUsuario' : conductor.idUsuario.id,
                        'nombre': conductor.idUsuario.nombre,
                        'apellidoPaterno':  conductor.idUsuario.apellidoPaterno,
                        'tarjeta':  conductor.tarjeta,
                        'numeroLicencia' : conductor.numeroLicencia
                    })
                cntx = {
                    'conductores': conductores
                }
                return JsonResponse(cntx)


            if request.POST.get('operation') == 'select':
                querySelect = sConductor.objects.filter(id=request.POST.get('id')).all().first()
                cntx = {
                    'id' : querySelect.id,
                    'CURP' : querySelect.idUsuario.CURP,
                    'tarjeta' : querySelect.tarjeta,
                    'numeroLicencia': querySelect.numeroLicencia,
                }
                return JsonResponse(cntx)


            if request.POST.get('operation') == 'update':
                try:
                    conductor = sConductor.objects.get(id=request.POST.get('id'))
                    conductor.tarjeta = request.POST.get('tarjeta')
                    conductor.numeroLicencia = request.POST.get('numeroLicencia')
                    conductor.save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})

            if request.POST.get('operation') == 'readUser':
                usersQuery = User.objects.values('id','CURP')
                users = []
                for user in usersQuery:
                    users.append({
                        'id' : user['id'],
                        'CURP'  : user['CURP']
                    })
                cntx = {
                    'users': users
                }
                return JsonResponse(cntx)

            if request.POST.get('operation') == 'create':
                print (int(request.POST.get('idUsuario')))
                try:
                    sConductor(
                        tarjeta=request.POST.get('tarjeta'),
                        numeroLicencia=request.POST.get('numeroLicencia'),
                        idUsuario=User.objects.get(id=request.POST.get('idUsuario')),
                    ).save()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})


            if request.POST.get('operation') == 'delete':
                try:
                    sVehiculo.objects.filter(id=request.POST.get('id')).delete()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})
