from django.contrib.auth import login, authenticate
from django.shortcuts import render
from django.views.generic import View
from iBus.models import sEstacion
from django.http import JsonResponse

class EstacionConfig(View):
    template_name = "config/estacion.html"
    def get(self, request):
        if not request.user.is_authenticated:
            return render(request, 'iBus/errors/403.html')
        else:
            pFlag = False
            return render(request, self.template_name)
            # perms = request.user.get_all_permissions()
            # for prm in perms:
            #     if prm == 'MESModeling.add_saudioalert':
            #         pFlag = True
            # if pFlag or request.user.is_staff:
            #     rutasQuerySet = sRuta.objects.all()
            #     cntx = {
            #         'rutas': rutasQuerySet
            #     }
            #     return render(request, 'iBus/rutas.html', cntx)
    
    def post(self, request):
        if not request.user.is_authenticated:
            return render(request, '403.html')
        else:
            if request.POST.get('operation') == 'read':
                estacionesQuery = sEstacion.objects.all()
                estaciones = []
                for estacion in estacionesQuery:
                    estaciones.append({
                        'id': estacion.id,
                        'aNombre': estacion.aNombre,
                        'aLatitud':  estacion.aLatitud,
                        'aLongitud': estacion.aLongitud,
                        'aCreated_at': estacion.aCreated_at,
                        'aUpdated_at': estacion.aUpdated_at
                    })
                cntx = {
                    'estaciones' : estaciones
                }
                return JsonResponse(cntx)

            if request.POST.get('operation') == 'select':
                estacionQuery = sEstacion.objects.filter(id=request.POST.get('id')).values('id', 'aNombre', 'aLatitud', 'aLongitud').first()
                cntx = {
                    'id' : estacionQuery['id'],
                    'aNombre' : estacionQuery['aNombre'],
                    'aLatitud': estacionQuery['aLatitud'],
                    'aLongitud': estacionQuery['aLongitud'],

                }
                return JsonResponse(cntx)

            if request.POST.get('operation') == 'update':
                try:
                    estacion = sEstacion.objects.get(id=request.POST.get('id'))
                    estacion.aNombre = request.POST.get('aNombre')
                    estacion.aLatitud = request.POST.get('aLatitud')
                    estacion.aLongitud = request.POST.get('aLongitud')
                    estacion.save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})

            
            if request.POST.get('operation') == 'create':
                try:
                    sEstacion(
                        aNombre = request.POST.get('aNombre'),
                        aLatitud = request.POST.get('aLatitud'),
                        aLongitud = request.POST.get('aLongitud')
                    ).save()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})

            if request.POST.get('operation') == 'delete':
                try:
                    sEstacion.objects.filter(id=request.POST.get('id')).delete()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})
                

        