from django.contrib.auth import login, authenticate
from django.shortcuts import render
from django.views.generic import CreateView
from iBus.models import sVehiculo
from django.http import JsonResponse


class VehiculoConfig(CreateView):
    template_name = "config/vehiculo.html"

    def get(self, request):
        if not request.user.is_authenticated:
            return render(request, 'iBus/errors/403.html')
        else:
            pFlag = False
            return render(request, self.template_name)

    def post(self, request):
        if not request.user.is_authenticated:
            return render(request, '403.html')
        else:

            if request.POST.get('operation') == 'read':
                vehiculosQuery = sVehiculo.objects.values('id', 'aModelo', 'aMarca', 'aMatricula', 'aNumeroEconomico')
                vehiculos = []
                for vehiculo in vehiculosQuery:
                    vehiculos.append({
                        'id': vehiculo['id'],
                        'aModelo': vehiculo['aModelo'],
                        'aMarca':  vehiculo['aMarca'],
                        'aMatricula':  vehiculo['aMatricula'],
                        'aNumeroEconomico' : vehiculo['aNumeroEconomico']
                    })
                cntx = {
                    'vehiculos': vehiculos
                }
                return JsonResponse(cntx)


            if request.POST.get('operation') == 'select':
                querySelect = sVehiculo.objects.filter(id=request.POST.get('id')).values('id', 'aModelo', 'aMarca','aMatricula', 'aNumeroEconomico').first()
                cntx = {
                    'id' : querySelect['id'],
                    'aModelo' : querySelect['aModelo'],
                    'aMarca': querySelect['aMarca'],
                    'aMatricula': querySelect['aMatricula'],
                    'aNumeroEconomico':querySelect['aNumeroEconomico']
                }
                return JsonResponse(cntx)


            if request.POST.get('operation') == 'update':
                try:
                    vehiculo = sVehiculo.objects.get(id=request.POST.get('id'))
                    vehiculo.aModelo = request.POST.get('aModelo')
                    vehiculo.aMarca = request.POST.get('aMarca')
                    vehiculo.aMatricula = request.POST.get('aMatricula')
                    vehiculo.aNumeroEconomico = request.POST.get('aNumeroEconomico')
                    vehiculo.save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})


            if request.POST.get('operation') == 'create':
                try:
                    sVehiculo(
                        aModelo=request.POST.get('aModelo'),
                        aMarca=request.POST.get('aMarca'),
                        aMatricula=request.POST.get('aMatricula'),
                        aNumeroEconomico=request.POST.get('aNumeroEconomico')
                    ).save()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})


            if request.POST.get('operation') == 'delete':
                try:
                    sVehiculo.objects.filter(id=request.POST.get('id')).delete()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})
